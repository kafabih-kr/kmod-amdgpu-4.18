# Description

This kernel module (DKMS) is from the latest 'amdgpu-dkms' package and ported to
4.18 Linux kernel.

# Disclaimer
This kmod is not belongs to me, I just ported it. Everythings that's happened to
you is YOUR OWN RISK. I AM NOT RESPONSIBILITY ABOUT WHAT HAPPENED ON YOUR
MACHINE.

# Changelogs
15/02/2019 ported amdgpu-dkms version 18.30-641594

# Installation
1. Clone or download this repo, if you download it, you must extract first.
2. Add it to DKMS as below

   <code>sudo dkms add ./amdgpu-((version))/</code>
3. Install it using DKMS as below

   <code>sudo dkms install amdgpu/((version)) --force</code>
4. Reboot or restart your machine to check are your Radeon is functioned.

# Uninstall
1. Uninstall it using DKMS as below

   <code>sudo dkms remove amdgpu/((version)) --all</code>
4. Reboot or restart your machine.
 
